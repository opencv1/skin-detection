#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

using namespace std;
using namespace cv;

#define dirTrainSkin  "trainset/skin"
#define dirTrainNonSkin  "trainset/nonskin"
#define dirTest  "testset"
#define imageExtension ".jpg"

void detect();
void getHistogramSkin();
void getHistogramNonSkin();
void writeHistogram();
void getHistogram(char *file,  bool isSkinExample);
vector<string> getTrainset(const char *dir);
int isImageFile(char *f);

int scale;
int totalPixelSkin = 0;
int totalPixelNonSkin = 0;
int totalPixel = 0;
float thres;
int histoS[256][256];
int histoNS[256][256];
char* nameImgTest;

int main(int argc, char ** argv){
    if(argc < 4){
        printf("Parammeter is not enough !");
        return 1;
    }
    scale = atol(argv[1]);
    thres = atof(argv[2]);
    nameImgTest = argv[3];
    getHistogramSkin();
    getHistogramNonSkin();
    writeHistogram();
    detect();
    return 0;
}

void detect(){
    char imageUrl[128];
    sprintf(imageUrl, "%s/%s", dirTest, nameImgTest);
    Mat img2;
    Mat img = imread(imageUrl, CV_LOAD_IMAGE_UNCHANGED);
    if(!img.data){
        printf("No data: %s\n", imageUrl );
    }

    cvtColor(img, img, CV_BGR2Lab);
    img.copyTo(img2);
    totalPixel = totalPixelSkin + totalPixelNonSkin;

    int nbRow = img.size().height;
    int nbCol = img.size().width;
    for (int row = 0; row < nbRow; row++){
        for (int col = 0; col < nbCol; col++){
            Vec3b labPixel = img.at<Vec3b>(row, col);

            int a = (int)labPixel.val[1]*scale / 256;
            int b = (int)labPixel.val[2]*scale / 256;
            double probabilitySkinColor = 1.0*histoS[b][a]/totalPixelSkin;
            double probabilityNonSkinColor = 1.0*histoNS[b][a]/totalPixelNonSkin;
            double probabilitySkin = 1.0*totalPixelSkin / totalPixel;
            double probabilityNonSkin = 1.0*totalPixelNonSkin / totalPixel;
            double probability = probabilitySkinColor * probabilitySkin / (probabilitySkin * probabilitySkinColor + probabilityNonSkin * probabilityNonSkinColor);

            labPixel.val[1] = 128;
            labPixel.val[2] = 128;
            labPixel.val[0] = 0;

            if(probability < thres){
                img.at<Vec3b>(row, col) = labPixel;
            }else{
                img2.at<Vec3b>(row, col) = labPixel;
            }
        }
    }
    cvtColor(img, img, CV_Lab2BGR);
    cvtColor(img2, img2, CV_Lab2BGR);

    char imageOutSkin[128];
    char imageOutNonSkin[128];
    sprintf(imageOutSkin, "output/skin/%s",  nameImgTest);
    sprintf(imageOutNonSkin, "output/nonskin/%s",  nameImgTest);
    imwrite(imageOutSkin, img);
    imwrite(imageOutNonSkin, img2);
}

void getHistogramSkin(){
    vector<string> listFile = getTrainset(dirTrainSkin);
    for (vector<string>::iterator it = listFile.begin() ; it != listFile.end(); ++it){
        char imageUrl[128];
        sprintf(imageUrl, "%s/%s", dirTrainSkin, (*it).c_str());

        Mat image = imread(imageUrl, CV_LOAD_IMAGE_UNCHANGED);
        cvtColor(image, image, CV_BGR2Lab);

        int nbRow = image.size().height;
        int nbCol = image.size().width;
        for (int row = 0; row < nbRow; row++){
            for (int col = 0; col < nbCol; col++){
                Vec3b labPixel = image.at<Vec3b>(row, col);

                if(labPixel.val[0] == 0 && labPixel.val[1] == 128 && labPixel.val[2] == 128){
                    continue;
                }
                int a = (int)labPixel.val[1]*scale / 256;
                int b = (int)labPixel.val[2]*scale / 256;
                histoS[b][a] ++;
                totalPixelSkin++;
            }
        }
    }
}

void getHistogramNonSkin(){
    vector<string> listFile = getTrainset(dirTrainNonSkin);
    for (vector<string>::iterator it = listFile.begin() ; it != listFile.end(); ++it){
        char imageUrl[128];
        sprintf(imageUrl, "%s/%s", dirTrainNonSkin, (*it).c_str());
        Mat image = imread(imageUrl, CV_LOAD_IMAGE_UNCHANGED);
        cvtColor(image, image, CV_BGR2Lab);

        int nbRow = image.size().height;
        int nbCol = image.size().width;
        for (int row = 0; row < nbRow; row++){
            for (int col = 0; col < nbCol; col++){
                Vec3b labPixel = image.at<Vec3b>(row, col);

                if(labPixel.val[0] == 0 && labPixel.val[1] == 128 && labPixel.val[2] == 128){
                    continue;
                }

                int a = (int)labPixel.val[1]*scale / 256;
                int b = (int)labPixel.val[2]*scale / 256;
                histoNS[b][a] ++;
                totalPixelNonSkin++;
            }
        }
    }
}



vector<string> getTrainset(const char *dir){
    DIR *dp;
    struct dirent *ep;
    vector<string> listFile;

    dp = opendir (dir);
    if (dp != NULL){
        while (ep = readdir (dp)){
            if(isImageFile(ep->d_name)){
                listFile.push_back(ep->d_name);
            }
        }

        (void) closedir (dp);
    }else{
        cout<<"Couldn't open the directory\n";
        exit(1);
    }

    return listFile;
}

int isImageFile(char *f){
    if(strstr(f, imageExtension) || strstr(f, ".jpg")){
        return 1;
    }
    return 0;
}


void writeHistogram(){
    Mat hist(scale + 1, scale + 1, CV_32FC1);
    Mat histN;
    hist.copyTo(histN);

    for (int row = 0; row < scale; row++){
        for (int col = 0; col < scale; col++){
            hist.at<double>(row, col) = 1.0*histoS[row][col]/totalPixelSkin;
        }
    }
    for (int row = 0; row < scale; row++){
        for (int col = 0; col < scale; col++){
            histN.at<double>(row, col) = 1.0*histoNS[row][col]/totalPixelNonSkin;
        }
    }
    char histName[64];
    char histNName[64];
    sprintf(histName, "histogram%d.jpg", scale);
    sprintf(histNName, "histogramNonSkin%d.jpg", scale);

    imwrite( histName, hist);
    imwrite( histNName, histN);
}
